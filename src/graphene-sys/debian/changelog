rust-graphene-sys (0.19.5-2) unstable; urgency=medium

  * Upload to unstable 

 -- Matthias Geiger <werdahias@riseup.net>  Sat, 04 May 2024 18:27:42 +0200

rust-graphene-sys (0.19.5-1) experimental; urgency=medium

  * Package graphene-sys 0.19.5 from crates.io using debcargo 2.6.1
  * Bump gir-rust-code-generator dependency to 0.19.1

 -- Matthias Geiger <werdahias@riseup.net>  Sat, 04 May 2024 13:40:29 +0200

rust-graphene-sys (0.19.0-2) experimental; urgency=medium

  * Fix dependency for libgraphene1.0

 -- Matthias Geiger <werdahias@riseup.net>  Mon, 26 Feb 2024 20:59:50 +0100

rust-graphene-sys (0.19.0-1) experimental; urgency=medium

  * Package graphene-sys 0.19.0 from crates.io using debcargo 2.6.1
  * Updated copyright years
  * Properly depend on packages needed for regenerization
  * Version dependency on gir-rust-code-generator
  * Add patch to downgrade pkgconfig dependency

 -- Matthias Geiger <werdahias@riseup.net>  Fri, 09 Feb 2024 21:27:06 +0100

rust-graphene-sys (0.18.1-2) unstable; urgency=medium

  * Team upload
  * Dop obsolete MSRV patch
  * Package graphene-sys 0.18.1 from crates.io using debcargo 2.6.0
  * Release to unstable

 -- Jeremy Bícha <jbicha@ubuntu.com>  Thu, 28 Sep 2023 14:43:51 -0400

rust-graphene-sys (0.18.1-1) experimental; urgency=medium

  * Package graphene-sys 0.18.1 from crates.io using debcargo 2.6.0
  * Included patch for MSRV downgrade
  * Marked v1_12 feature as flaky for now as the C library is too old
  * Regenerate source code with debian tools before build

 -- Matthias Geiger <werdahias@riseup.net>  Fri, 08 Sep 2023 20:31:17 +0200

rust-graphene-sys (0.17.10-1) unstable; urgency=medium

  * Package graphene-sys 0.17.10 from crates.io using debcargo 2.6.0
  * Removed inactive uploader, added my new mail address
  * Drop 0001-graphene-fix-alignment-of-graphene_simd4f_t-on-
    armhf.patch, rebase fix-autopkgtest-on-x32-arches.diff

 -- Matthias Geiger <werdahias@riseup.net>  Tue, 01 Aug 2023 23:56:13 +0200

rust-graphene-sys (0.16.3-6) unstable; urgency=medium

  * Package graphene-sys 0.16.3 from crates.io using debcargo 2.6.0

 -- Matthias Geiger <matthias.geiger1024@tutanota.de>  Sun, 25 Jun 2023 20:36:55 +0200

rust-graphene-sys (0.16.3-5) experimental; urgency=medium

  * Package graphene-sys 0.16.3 from crates.io using debcargo 2.6.0
  * Use correct syntax for the patch

 -- Matthias Geiger <matthias.geiger1024@tutanota.de>  Mon, 29 May 2023 13:36:52 +0200

rust-graphene-sys (0.16.3-4) experimental; urgency=medium

  * Package graphene-sys 0.16.3 from crates.io using debcargo 2.6.0
  * Fix typo in patch

 -- Matthias Geiger <matthias.geiger1024@tutanota.de>  Mon, 29 May 2023 09:14:49 +0200

rust-graphene-sys (0.16.3-3) experimental; urgency=medium

  * Package graphene-sys 0.16.3 from crates.io using debcargo 2.6.0

 -- Matthias Geiger <matthias.geiger1024@tutanota.de>  Sun, 28 May 2023 20:10:16 +0200

rust-graphene-sys (0.16.3-2) experimental; urgency=medium

  * Team upload.
  * d/p/0001-graphene-fix-alignment-of-graphene_simd4f_t-on-armhf.patch:
    fix alignment issues on armhf and s390x. (LP: #2020902)

 -- Simon Chopin <schopin@ubuntu.com>  Sat, 27 May 2023 10:22:57 +0200

rust-graphene-sys (0.16.3-1) experimental; urgency=medium

  * Package graphene-sys 0.16.3 from crates.io using debcargo 2.6.0
  * Collapsed features in debcargo.toml
  * Added myself to Uploaders
  * Dropped obsolete patches

 -- Matthias Geiger <matthias.geiger1024@tutanota.de>  Sat, 20 May 2023 13:26:29 +0200

rust-graphene-sys (0.14.8-2) unstable; urgency=medium

  * Team upload.
  * debian/patches: Update to system-deps 6

 -- Sebastain Ramacher <sramacher@debian.org>  Wed, 05 Oct 2022 09:39:36 +0200

rust-graphene-sys (0.14.8-1) unstable; urgency=medium

  * Package graphene-sys 0.14.8 from crates.io using debcargo 2.4.4

 -- Henry-Nicolas Tourneur <debian@nilux.be>  Sun, 05 Dec 2021 22:56:11 +0100
