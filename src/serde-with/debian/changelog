rust-serde-with (3.8.1-1) unstable; urgency=medium

  * Team upload.
  * Package serde_with 3.8.1 from crates.io using debcargo 2.6.1
  * Update patches for new upstream.
  * Skip some order-sensitive tests on big endian systems.
  * Revert upstream bump of base64 dependency.

 -- Peter Michael Green <plugwash@debian.org>  Sat, 25 May 2024 10:27:34 +0000

rust-serde-with (3.6.1-1) unstable; urgency=medium

  * Team upload.
  * Package serde_with 3.6.1 from crates.io using debcargo 2.6.1
  * Update patches for new upstream.
  * Disable a few more oversensitive tests.
  * Enable hashbrown 0.14 and indexmap 2 features.
  * Disable indexmap 1 feature.
  * Remove dev-dependency on jsonschema and disable tests that depend on it
    so the rest of the testsuite can run.
  * Relax dev-dependency on regex.

 -- Peter Michael Green <plugwash@debian.org>  Thu, 16 May 2024 05:15:40 +0000

rust-serde-with (3.4.0-3) unstable; urgency=medium

  * Team upload.
  * Package serde_with 3.4.0 from crates.io using debcargo 2.6.1
  * Backport test fix for serde >= 1.0.196

 -- James McCoy <jamessan@debian.org>  Fri, 03 May 2024 07:48:10 -0400

rust-serde-with (3.4.0-2) unstable; urgency=medium

  * Team upload.
  * Package serde_with 3.4.0 from crates.io using debcargo 2.6.1
  * Disable tests that are sensitive to hashset/hashmap order, it seems to
    vary with different endian.

 -- Peter Michael Green <plugwash@debian.org>  Sat, 09 Dec 2023 20:35:02 +0000

rust-serde-with (3.4.0-1) unstable; urgency=medium

  * Team upload.
  * Package serde_with 3.4.0 from crates.io using debcargo 2.6.1
  * Rearrange patches for easier testing and update them for new upstream.
  * Fix autopkgtests
    + Disable actual usage of document-features as well as the dependency.
    + Really disable indexmap2 tests.
    + Disable tests that are overly sensitive to the precise output of
      external crates.
    + Disable test that only makes sense in the context of the upstream
      workspace.
    + Relax dev-dependencies to allow the versions currently in Debian.
    + Fix feature requirements for tests.

 -- Peter Michael Green <plugwash@debian.org>  Thu, 07 Dec 2023 16:04:27 +0000

rust-serde-with (3.3.0-1) unstable; urgency=medium

  * Package serde_with 3.3.0 from crates.io using debcargo 2.6.0

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 23 Sep 2023 04:07:09 +0100
